from models.movie import Movie as MovieModel
from schemas.movie import Movie

class MovieService():
    def __init__(self, db) -> None:
        self.db = db

    def get_movies(self):
        res = self.db.query(MovieModel).all()
        return res
    
    def get_movie(self, id):
        res = self.db.query(MovieModel).filter(MovieModel.id == id).first()
        return res

    def get_movies_by_category(self, category):
        res = self.db.query(MovieModel).filter(MovieModel.category == category).all()
        return res

    def create_movie(self, movie: Movie):
        new_movie = MovieModel(**movie.dict())
        self.db.add(new_movie)
        self.db.commit()
        return

    def update_movie(self, id, new_movie: Movie):
        movie = self.get_movie(id)
        movie.title = new_movie.title
        movie.year = new_movie.year
        movie.rating = new_movie.rating
        movie.overview = new_movie.overview
        movie.category = new_movie.category
        self.db.commit()
        return

    def delete_movie(self, id):
        self.db.query(MovieModel).filter(MovieModel.id == id).delete()
        self.db.commit()
        return