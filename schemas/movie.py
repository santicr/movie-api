from typing import Optional
from pydantic import BaseModel, Field

class Movie(BaseModel):
    id: Optional[int] = None
    title: str = Field(min_length = 5, max_length = 15)
    overview: str = Field(min_length = 15, max_length = 50)
    year: int = Field(le = 2023, ge = 1990)
    rating: float = Field(le = 10, ge = 0)
    category: str = Field(min_length = 3, max_length = 30)

    class Config:
        schema_extra = {
            "example" : {
                "id": 1,
                "title": 'Mi película',
                "overview": "Descripción de la película",
                "year": 2020,
                "rating": 9.6,
                "category": "Acción"
            }
        }