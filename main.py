from fastapi import FastAPI
from fastapi.responses import HTMLResponse
from config.db import Base, engine
from middlewares.error_handler import ErrorHandler
from routers.movie import movie_router
from routers.user import user_router

app = FastAPI()
app.title = "Primera aplicación con Fast API"
app.version = "0.0.1"

app.include_router(user_router)
app.include_router(movie_router)

app.add_middleware(ErrorHandler)

Base.metadata.create_all(bind = engine)

@app.get('/', tags = ['Home'])
def message():
    return HTMLResponse('<h1>Hello world</h1>')