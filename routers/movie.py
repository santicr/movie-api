from fastapi import APIRouter, Path, Query, Depends
from fastapi.responses import JSONResponse
from typing import List
from config.db import Session
from models.movie import Movie as MovieModel
from fastapi.encoders import jsonable_encoder
from middlewares.jwt_bearer import JWTBearer
from services.movie import MovieService
from schemas.movie import Movie

movie_router = APIRouter(
    tags=['Movies']
)

@movie_router.get('/movies', response_model = List[Movie], status_code = 200)
def get_movies() -> List[Movie]:
    db = Session()
    result = MovieService(db).get_movies()
    return JSONResponse(status_code = 200, content = jsonable_encoder(result))

@movie_router.get('/movies/{id}', response_model = Movie)
def get_movie(id: int = Path(ge = 1, le = 2000)) -> Movie:
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code = 404, content = {'message': 'No encontrado'})
    return JSONResponse(status_code = 200, content = jsonable_encoder(result))
    
@movie_router.get('/movies/', response_model = List[Movie], status_code = 200)
def get_movies_by_category(category: str = Query(min_length = 3, max_length = 30)) -> List[Movie]:
    db = Session()
    result = MovieService(db).get_movies_by_category(category)
    if result:
        return JSONResponse(status_code = 200, content = jsonable_encoder(result))
    return JSONResponse(status_code = 404, content = {'message': 'No se han encontrado peliculas'})

@movie_router.post('/movies', response_model = dict, status_code = 201)
def create_movie(movie: Movie) -> dict:
    db = Session()
    MovieService(db).create_movie(movie)
    return JSONResponse(status_code = 201, content = {'message': 'Se ha registrado correctamente una película'})

@movie_router.put('/movies/{id}', response_model = dict, status_code = 200)
def update_movie_by_id(id: int, new_movie: Movie) -> dict:
    db = Session()
    if not MovieService(db).get_movie(id):
        return JSONResponse(status_code = 404, content = {'message': 'Película no encontrada'})
    MovieService(db).update_movie(id, new_movie)
    return JSONResponse(status_code = 200, content = {'message': 'Se ha modificado la película'})

@movie_router.delete('/movies/{id}', response_model = dict, status_code = 200)
def delete_movie_by_id(id: int) -> dict:
    db = Session()
    if not MovieService(db).get_movie(id):
        return JSONResponse(status_code = 200, content = {'message': 'No se ha encontrado la película'})
    MovieService(db).delete_movie(id)
    return JSONResponse(status_code = 200, content = {'message': 'Se ha eliminado correctamente la película'})